'use strict';

app.home = kendo.observable({
    dataSource: new kendo.data.DataSource({
        data: [
            { Uri: 'images/01.jpg' },
            { Uri: 'images/02.jpg' },
            { Uri: 'images/03.jpg' },
            { Uri: 'images/04.jpg' },
            { Uri: 'images/05.jpg' },
        ]
    }),
    currentPage: 0,
    onShow: function(e) {
        var itemUid = e.view.params.uid;
        var dataSource = app.home.get('dataSource');
        var data = dataSource.view();
        var runSlideshow = true;
        var length = data.length;

        console.log(data);

        var scrollview = $("#scrollview").data("kendoMobileScrollView");

        slideshow(length);

        function slideshow(arrayLength) {
            setTimeout(function() {
                var nextPage = scrollview.page + 1;
                if (nextPage >= arrayLength) {
                    scrollview.scrollTo(0);
                } else {
                    scrollview.next();
                }

                if (runSlideshow) {
                    slideshow(arrayLength);
                }
            }, 4000);
        }
    },
    onChange: function(e) {
        app.home.set("currentPage", e.nextPage);
    },
});
app.localization.registerView('home');

// START_CUSTOM_CODE_home
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
document.addEventListener("deviceready", onDeviceReady, false);

//function onPushNotificationReceived(e) {
//  alert(JSON.stringify(e));
//};

var onAndroidPushReceived = function(e) {
    var message = e.message;
    alert(message);
};

var onWpPushReceived = function(e) {
    if (e.type === "toast" && e.jsonContent) {
        var message = e.jsonContent["wp:Text2"] || e.jsonContent["wp:Text1"];
        alert(message);
    }
};

var onIosPushReceived = function(e) {
    var message = e.alert;
    navigator.notification.alert(message, alertCallback, ["Title"], [buttonName])
        //alert(message);
};

function onDeviceReady() {
    var everlive = new Everlive({
        appId: '7z4gvjouddgf8idc',
        scheme: 'https'
    });
    var devicePushSettings = {
        iOS: {
            badge: 'true',
            sound: 'true',
            alert: 'true'
        },
        android: {
            senderID: '394984770827'
        },
        wp8: {
            channelName: 'EverlivePushChannel'
        },
        notificationCallbackWP8: onWpPushReceived,
        notificationCallbackAndroid: onAndroidPushReceived,
        notificationCallbackIOS: onIosPushReceived
    };
    everlive.push.register(devicePushSettings, function() {
        //alert("Successful registration in Telerik Platform. You are ready to receive push notifications.");
    }, function(err) {
        alert("Error: " + err.message);
    });
}

function openRegFromHomePage() {
    window.open('http://thevillagegroup.org/after-school-programs/', '_system', 'location=no,hidden=yes');
}

function openDonFromHomePage() {
    window.open('http://thevillagegroup.org/community-programs-and-services/donate/', '_system', 'location=no,hidden=yes');
}


// END_CUSTOM_CODE_home
